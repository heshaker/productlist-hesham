import { Route, Routes } from "react-router-dom";
import { AddProduct } from "./components/add-product/AddProduct";
import { ProductList } from "./components/products-list/ProductList";
import { ProductsService } from "./services/ProductService";
import { BrowserRouter } from "react-router-dom";
import "./App.scss";

const ProductService = new ProductsService();

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<ProductList service={ProductService} />} />
          <Route
            path="/add-product"
            element={<AddProduct service={ProductService} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
