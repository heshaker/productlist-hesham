import { ProductModel } from "../models/ProductModel";
import axios from "axios";

const config = {
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json",
  },
};

export class ProductsService {
  getAllProducts = async () => {
    try {
      const response = await axios.get(
        window.storeEnv.server_url + "/api/products",
        config
      );
      if (response && (response.statusText === "OK" || response.status == 200))
        if (response && response.data) {
          return response.data.map((product) => {
            return new ProductModel(product);
          });
        }
    } catch (error) {
      console.error(error);
    }
  };

  getAllTypes = async () => {
    try {
      const response = await axios.get(
        window.storeEnv.server_url + "/api/types",
        config
      );
      if (response && (response.statusText === "OK" || response.status == 200))
        if (response && response.data) {
          return response.data;
        }
    } catch (error) {
      console.error(error);
    }
  };

  massDelete = async (checkedProductsIds) => {
    if (!checkedProductsIds.length) {
      alert("Please check products you want to delete");
    } else {
      try {
        const response = await axios.post(
          window.storeEnv.server_url + "/api/products/mass-delete",
          JSON.stringify(checkedProductsIds),
          config
        );
        return response;
      } catch (error) {
        console.error(error);
      }
    }
  };

  saveProduct = async (product) => {
    try {
      const response = await axios.post(
        window.storeEnv.server_url + "/api/products",
        JSON.stringify(product),
        config
      );
      return response;
    } catch (error) {
      console.error(error);
    }
  };
}
