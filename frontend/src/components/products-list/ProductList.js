import React from "react";
import { Header } from "./Header";
import { ProductSingle } from "./ProductSingle";
import "./ProductsList.scss";

export function ProductList({ service }) {
  const [products, setProducts] = React.useState([]);
  const [checkedProducts, setCheckedProducts] = React.useState([]);

  React.useEffect(() => {
    service.getAllProducts().then((productModels) => {
      if (productModels && productModels.length > 0) {
        setProducts(productModels);
      }
    });
  }, [service]);

  const onCheckProduct = (product, isChecked) => {
    product.setChecked(isChecked);
    if (isChecked) {
      setCheckedProducts([...checkedProducts, product.id]);
    } else {
      const index = checkedProducts.findIndex((id) => id === product.id);
      if (index >= 0) {
        checkedProducts.splice(index, 1);
        setCheckedProducts([...checkedProducts]);
      }
    }
  };

  const onMassDelete = () => {
    if (checkedProducts && checkedProducts.length > 0) {
      service.massDelete(checkedProducts).then((response) => {
        if (
          response &&
          (response.statusText === "OK" || response.status == 200)
        ) {
          checkedProducts.forEach((id) => {
            const index = products.findIndex((product) => product.id === id);
            if (index >= 0) {
              products.splice(index, 1);
            }
          });
          setProducts([...products]);
          setCheckedProducts([]);
        }
      });
    } else {
      alert("Please check products you want to delete");
    }
  };

  return (
    <div className="products-list-container">
      <Header service={service} onMassDelete={onMassDelete} />
      <hr />
      <div className="products-container">
        {products.map((product) => {
          return (
            <ProductSingle
              key={product.id}
              product={product}
              service={service}
              onCheck={onCheckProduct}
            />
          );
        })}
      </div>
    </div>
  );
}
