import React from "react";

export function ProductSingle({ product, onCheck }) {
  const onCheckProduct = React.useCallback(
    ({ target: { checked } }) => {
      onCheck(product, checked);
    },
    [product, onCheck]
  );

  if (!product) return null;

  const isFurniture = Object.keys(product.attributes).length > 1;

  return (
    <div className="product-container">
      <div className="product">
        <div className="">
          <input
            className="delete-checkbox"
            type={"checkbox"}
            onChange={onCheckProduct}
          />
        </div>
        <p>{product.sku}</p>
        <p>{product.name}</p>
        <p>{product.price}</p>
        <div className="product-attributes-list">
          {isFurniture && (
            <p>
              Dimension:{" "}
              {Object.values(product.attributes).join(product.unit + "x") +
                product.unit}
            </p>
          )}
          {!isFurniture &&
            Object.keys(product.attributes).map((attr) => {
              return (
                <p>
                  <span style={{ textTransform: "capitalize" }}>{attr}</span>:{" "}
                  {product.attributes[attr]}
                  {product.unit}
                </p>
              );
            })}
        </div>
      </div>
    </div>
  );
}
