import React from "react";
import { useNavigate } from "react-router-dom";

export function Header({ service, onMassDelete }) {
  const navigate = useNavigate();
  const onAddNewProduct = React.useCallback(
    (event) => {
      navigate("/add-product");
    },
    [navigate]
  );

  return (
    <div className="header-container">
      <h1 className="header-title">Product List</h1>
      <div className="action-container">
        <button className="btn-add" onClick={onAddNewProduct}>
          Add
        </button>
        <button
          id="delete-product-button"
          className="btn-delete"
          onClick={onMassDelete}
        >
          Delete
        </button>
      </div>
    </div>
  );
}
