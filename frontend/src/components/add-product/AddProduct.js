import React from "react";
import { useNavigate } from "react-router-dom";
import "./AddProduct.scss";

export function AddProduct({ service }) {
  const navigate = useNavigate();
  const [types, setTypes] = React.useState([]);
  const [product, setProduct] = React.useState({
    sku: "",
    name: "",
    price: "",
    type_id: "",
    type_name: "",
    attributes: {
      height: "",
      width: "",
      length: "",
    },
  });

  React.useEffect(() => {
    service.getAllTypes().then((typesData) => {
      if (typesData && typesData.length) {
        setTypes(typesData);
      }
    });
  }, []);

  const onSubmit = (event) => {
    if (event) event.preventDefault();
    service.saveProduct(product).then((response) => {
      console.log(response);
      if (
        response &&
        (response.statusText === "OK" || response.status == 200)
      ) {
        navigate("/");
      }
    });
  };

  const onCancel = () => {
    navigate("/");
  };

  const onInputChange = ({ target: { value, name } }) => {
    if (name === "type_id") {
      product.attributes = {};
      const t = types.find((type) => type.id === value);
      product.type_name = t ? t.name : null;
    }
    setProduct({ ...product, [name]: value });
  };

  const onAttributeChange = ({ target: { value, name } }) => {
    product.attributes[name] = value;
    setProduct({ ...product });
  };

  return (
    <div className="add-product-container">
      <form id="product_form" onSubmit={onSubmit}>
        <div className="header">
          <h1>Add Product</h1>
          <div className="add-buttons-container">
            <button type="submit" onClick={onSubmit}>
              Save
            </button>
            <button type="reset" onClick={onCancel}>
              Cancel
            </button>
          </div>
        </div>
        <hr />
        <div className="add-product-section">
          <InputLabel
            name={"sku"}
            label="Sku"
            onChange={onInputChange}
            id="sku"
            value={product.sku}
          />
          <InputLabel
            name={"name"}
            label="Name"
            onChange={onInputChange}
            id="name"
            value={product.name}
          />
          <InputLabel
            name={"price"}
            label="Price($)"
            onChange={onInputChange}
            id="price"
            value={product.price}
          />
          <div className="label-input">
            <label htmlFor={"productType"}>Type Switcher</label>
            <select
              id="productType"
              name="type_id"
              onChange={onInputChange}
              value={product.type_id}
            >
              <option value={undefined}>Type</option>
              {types.map((type, index) => {
                return (
                  <option key={type.id + index} value={type.id}>
                    {type.name}
                  </option>
                );
              })}
            </select>
          </div>

          {product.type_name === "dvd" && (
            <div id="dvd">
              <InputLabel
                name={"size"}
                label="Size"
                onChange={onAttributeChange}
                id="size"
                value={product.attributes.size}
              />
              <p className="red">Please provide DVD size</p>
            </div>
          )}

          {product.type_name === "book" && (
            <div id="book">
              <InputLabel
                name={"weight"}
                label="Weight"
                onChange={onAttributeChange}
                id="weight"
                value={product.attributes.weight}
              />
              <p className="red">Please provide the book weight</p>
            </div>
          )}

          {product.type_name === "furniture" && (
            <div id="furniture">
              <InputLabel
                name={"height"}
                label="Height"
                onChange={onAttributeChange}
                id="height"
                value={product.attributes.height}
              />
              <InputLabel
                name={"width"}
                label="Width"
                onChange={onAttributeChange}
                id="width"
                value={product.attributes.width}
              />
              <InputLabel
                name={"length"}
                label="Length"
                onChange={onAttributeChange}
                id="length"
                value={product.attributes.length}
              />
              <p className="red">Please provide the product dimensions HxWxL</p>
            </div>
          )}
        </div>
      </form>
    </div>
  );
}

function InputLabel({ label, name, onChange, id, value, type = "text" }) {
  return (
    <div className="label-input">
      <label htmlFor={id}>{label}</label>
      <input
        type={type}
        id={id}
        value={value}
        onChange={onChange}
        name={name}
      />
    </div>
  );
}
