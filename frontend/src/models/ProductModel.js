import shortid from "shortid";
export class ProductModel {
  id = shortid.generate();
  sku = null;
  name = null;
  price = null;

  type_id = null;
  type_name = null;
  unit = null;

  attributes = null;

  checked = false;

  constructor(payload = null) {
    if (payload && Object.keys(payload).length) {
      for (let key in payload) {
        if (key in this) {
          this[key] = payload[key];
        }
      }
    }
  }

  setChecked = (value) => {
    this.checked = value;
    return this.checked;
  };

  toJson = () => {
    return {
      name: this.name,
      sku: this.sku,
      price: this.price,
      attributes: this.attributes,
      type_id: this.type_id,
    };
  };
}
