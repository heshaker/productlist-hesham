<?php

require __DIR__ . '/models/ProductModel.php';

use Bramus\Router\Router;
use Hesham\Store\Models\ProductModel;


require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/credentials.php';

$router = new Router();



header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: *');
header('content-length: 52098');


$router->match('GET|OPTIONS', '/api/products', function () {
    $results = [];
    try {
        $ProductProvider = new ProductModel();
        $results = $ProductProvider->selectAll(true);
    } catch (Exception $error) {
        echo json_encode([
            'error' => 'Could not get products',
            'status' => 500,
            'ok' => false,
            'message' => $error->getMessage(),
        ]);
    }

    echo json_encode($results);
});

$router->match('POST|OPTIONS', '/api/products', function () {
    $post = json_decode(file_get_contents('php://input'));

    $post->attributes = json_encode($post->attributes);

    $ProductProvider = new ProductModel($post);
    try {
        $results = $ProductProvider->save();
    } catch (Exception $error) {
        echo json_encode([
            'error' => 'Could not save product',
            'status' => 500,
            'ok' => false,
            'message' => $error->getMessage(),
        ]);
        throw new Error('Could not save to database', 500);
    }

    echo json_encode($results);
});


$router->match('GET|OPTIONS', '/api/types', function () {

    $results = [];
    try {
        $ProductProvider = new ProductModel();
        $results = $ProductProvider->selectAllTypes();
    } catch (Exception $error) {
        echo json_encode([
            'error' => 'Could not get products',
            'status' => 500,
            'ok' => false,
            'message' => $error->getMessage(),
        ]);
    }

    echo json_encode($results);
});


$router->match('POST|OPTIONS', '/api/products/mass-delete', function () {

    $ids = json_decode(file_get_contents('php://input'));

    $results = $ids;

    if ($ids) {
        try {
            $ProductProvider = new ProductModel();
            $results = $ProductProvider->massDelete($ids);
        } catch (Exception $error) {
            echo json_encode([
                'error' => 'Could not delete products',
                'status' => 500,
                'ok' => false,
                'message' => $error->getMessage(),
            ]);
            throw new Error('Could not delete products', 500);
        }
    }
    echo json_encode($results);
});



$router->run();
