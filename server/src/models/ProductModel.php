<?php

namespace Hesham\Store\Models;

require __DIR__ . '/DataProvider.php';

class ProductModel extends DataProvider
{
    protected $table = 'products';
    protected $types_table = 'product_types';

    public $id;
    public $name;
    public $type_id;
    public $type_name;
    public $sku;
    public $price;
    public $attributes;
    public $unit;

    public function __construct($data = null)
    {
        parent::__construct();
        if ($data)
            foreach ($data as $key => $value) {
                if (property_exists($this, $key)) {
                    $this->{$key} = $value;
                }
            }
    }

    public function buildFromDataBase($data)
    {
        $this->id = $data['id'];
        $this->price = $data['price'];
        $this->sku = $data['sku'];
        $this->name = $data['name'];
        $this->type_id = $data['type_id'];
        $this->type_name = $data['type_name'];
        $this->unit = $data['unit'];
        $this->attributes = $data['attributes'];
        return $this;
    }


    public function selectAll($json = false)
    {
        $query = "SELECT p.*, t.name as type_name, t.unit FROM {$this->table} as p 
        JOIN  {$this->types_table} as t ON p.type_id = t.id;";

        $query = $this->connection->query($query);
        $results = [];
        while ($obj = $query->fetch_assoc()) {
            if ($json) {
                $product = new ProductModel();
                $product->buildFromDataBase($obj);
                $results[] =  $product->toAssociateArray();
            } else {
                $product = new ProductModel();
                $product->buildFromDataBase($obj);
                $results[] =  $product;
            }
        }
        return $results;
    }

    public function selectAllTypes()
    {
        $query = "SELECT * FROM {$this->types_table};";

        $query = $this->connection->query($query);
        $results = [];
        while ($obj = $query->fetch_assoc()) {
            $results[] = $obj;
        }
        return $results;
    }

    public  function  save()
    {
        $query = "INSERT INTO `products` (`id`, `sku`, `name`, `price`, `type_id`, `attributes`) 
        VALUES (null, '$this->sku', '$this->name', '$this->price', '$this->type_id', '$this->attributes');";
        $query = $this->connection->query($query);
        return $this->toAssociateArray();
    }

    public function massDelete($productIds)
    {
        $list =  join(',', $productIds);
        $query = "DELETE FROM `products` WHERE `products`.`id` in ($list)";
        $query = $this->connection->query($query);
        return $productIds;
    }

    public function toAssociateArray()
    {

        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'sku' => $this->sku,
            'attributes' => json_decode($this->attributes),
            'type_id' => $this->type_id,
            'type_name' => $this->type_name,
            'unit' => $this->unit,
        ];
    }

    public function toJson()
    {
        return json_encode($this->toAssociateArray());
    }
}
