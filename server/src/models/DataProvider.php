<?php

namespace Hesham\Store\Models;

use mysqli;

abstract class DataProvider
{
    protected $connection;

    public function __construct()
    {
        $this->connection = new mysqli(DATA_BASE_SERVER, DATA_BASE_USER, DATA_BASE_PASSWORD, DATA_BASE_NAME);
        if ($this->connection->connect_error) {
            die("Connection failed: " . $this->connection->connect_error);
        }
    }

    abstract  function selectAll();
    abstract  function save();
    abstract  function selectAllTypes();
    abstract function massDelete($productIds);
}
